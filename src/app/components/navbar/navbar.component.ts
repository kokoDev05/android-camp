import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLoggedIn;
  isAdmin: any;
  public isCollapsed = true;
  // pruebas
  subscription: Subscription;
  message: any;

  constructor(private auth: AuthService, private router: Router) {
    this.auth.authState.subscribe(exito => {
      this.isLoggedIn = exito;
      // this.isAdmin = this.auth.isAdmin;
      // console.log(this.auth.isAdmin);
    });
    // pruebas
    this.subscription = this.auth.getMessage().subscribe(message => {
      this.isAdmin = message.text;
      //  console.log(this.message.text);
    });
   }
  ngOnInit() {
  }
  iniciarSesion() {
    this.auth.login();
  }
  cerrarSesion() {
    this.auth.logout();
    this.router.navigate(['']);
  }
  prevent($event) {
    $event.preventDefault();
  }
}
