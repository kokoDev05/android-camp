import { Component, OnInit } from '@angular/core';
import { FireService } from './../../services/fire.service';
import { Subject } from 'rxjs/Subject';
import { debounceTime } from 'rxjs/operator/debounceTime';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-acreditar-grupo',
  templateUrl: './acreditar-grupo.component.html',
  styleUrls: ['./acreditar-grupo.component.css']
})
export class AcreditarGrupoComponent implements OnInit {
  tamanoDeGrupo = 3;
  searchText;
  filtro = true;
  inscritos: any[];
  inscrito;
  proceder = false;
  grupo: any[];
  grupoCompleto;
  numero_grupo = 0;
  numero_vacio = true;
  // alerta
  private _success = new Subject<string>();
  staticAlertClosed = false;
  successMessage: string;
  // guias
  seleccionado = false;
  guiaSelecionado;
  totalguias;
  guias: any[] = [];
  // ventana modal
  modalOption: NgbModalOptions = {};
  closeResult: string;
  constructor(private fire: FireService, private modalService: NgbModal, private router: Router) {
    this.grupo = [];
    this.grupoCompleto = false;
    this.fire.getInscritos().snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(inscritos => {
        this.inscritos = inscritos;
      });
      // guias
      this.fire.getGuias().snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
      .subscribe(guias => {
        this.totalguias = guias;
        this.guias = guias;
        // console.log(this.guias.length);
      });
   }

  ngOnInit() {
    // alerta
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    debounceTime.call(this._success, 3000).subscribe(() => this.successMessage = null);
  }

  cambiarFiltro() {
    this.filtro = !this.filtro;
  }
  seleccionar($event, inscrito) {
    $event.preventDefault();
    this.inscrito = inscrito;
    this.proceder = false;
    if (!(this.numero_grupo === 0)) {
      if (this.grupo.includes(inscrito)) {
        console.log('lo incluye');
        this._success.next('El inscrito ya esta en el grupo');
      } else {
        this.grupo.push(inscrito);
      }
      if (!(this.grupo.length < this.numero_grupo)) {
        this.grupoCompleto = true;
        console.log(this.grupo.length);
      }
    }else {
      this.numero_vacio = true;
    }
  }
  buscar() {
    this.proceder = true;
    this.seleccionado = false;
  }
  volveraBuscar() {
    // this.proceder = true;
    this.seleccionado = false;
  }
  guiaRandom(guias) {
    //numero de participantes
    do {
      this.guiaSelecionado = guias[Math.floor(Math.random() * guias.length)];
    } while (((this.guiaSelecionado.num_participantes + this.numero_grupo) < this.tamanoDeGrupo)
     && this.guiaSelecionado.num_participantes < this.tamanoDeGrupo);
    this.seleccionado = true;
  }
  filtrar(num) {
    const guiasFiltrados = [];
    // value.forEach(guia => {
    //   if (guia.num_participantes < this.tamanoDeGrupo) {
    //     guiasFiltrados.push(guia);
    //   }
    // });
    // console.log(num);
    this.totalguias.forEach(guia => {
      if ((guia.num_participantes + num) <= this.tamanoDeGrupo) {
        guiasFiltrados.push(guia);
      }
    });
    // console.log(guiasFiltrados.length);
    return guiasFiltrados;
  }
  // filtrar2(value) {
  //   const guiasFiltrados = [];
  //   value.forEach(guia => {
  //     if ((this.tamanoDeGrupo - guia.num_participantes) >= this.numero_grupo) {
  //       guiasFiltrados.push(guia);
  //     }
  //   });
  //   console.log('filtrado');
  //   return guiasFiltrados;
  // }
  seleccionarGuia(guia, $event?) {
    this.seleccionado = true;
    this.guiaSelecionado = guia;
    this.proceder = false;
  }
  open(content, guia) {
    if (guia != null && this.grupo.length > 0) {

      // acreditar grupo
      // this.fire.agregarParticipante(guia);
      // console.log(guia);
      this.grupo.forEach(element => {
        // console.log(element);
        this.fire.acreditar(element, guia);
        this.fire.agregarParticipante(guia);
      });

      this.modalOption.backdrop = 'static';
      this.modalOption.keyboard = false;
      this.modalService.open(content, this.modalOption).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        this.router.navigate(['puntos']);
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        this.router.navigate(['']);
      });
    } else {
      console.log('hi!');
    }
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  cancelar() {
    console.log('cancelar');
    this.router.navigate(['']);
  }
  cambiarNumero($event) {
    const val = $event.target.value;
    if (val !== '') {
      if (val < this.numero_grupo) {
        this.grupo = [];
        this.numero_grupo = val;
        this.grupoCompleto = false;
        this.proceder = false;
        this.numero_vacio = false;
        this.guias = this.totalguias;
        this.guias = this.filtrar(Number(val));
        console.log('menor grupo');
      }else {
        this.numero_grupo = val;
        this.grupoCompleto = false;
        this.numero_vacio = false;
        this.guias = this.totalguias;
        this.guias = this.filtrar(Number(val));
        // console.log('mayor grupo');
      }
      // console.log(this.guias.length);
      // this.guias = this.filtrar2(this.guias);
    } else {
      this.numero_vacio = true;
    }
  }
}
