import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Inscrito } from '../../models/inscrito';

import { DatosService } from '../../services/datos.service';
import { FireService } from '../../services/fire.service';


@Component({
  selector: 'app-lista-inscritos',
  templateUrl: './lista-inscritos.component.html',
  styleUrls: ['./lista-inscritos.component.css']
})
export class ListaInscritosComponent implements OnInit {

  inscritos: any[];
  message: Inscrito;
  filtro = true;
  searchText;


  constructor(private data: DatosService, private fire: FireService, private router: Router) {
   }

  ngOnInit() {
    this.fire.getInscritos().snapshotChanges()
    .map(changes => {
      return changes.map( c => ({key: c.payload.key, ...c.payload.val()}));
    })
    .subscribe(inscritos => this.inscritos = inscritos);
  }


  acreditar(event, inscrito: Inscrito) {
    this.router.navigate(['acreditar-inscrito']);
    this.data.changeMessage(inscrito);
  }

  cambiarFiltro() {
    this.filtro = !this.filtro;
  }
}
